#input = sequence of letter notes and digits
# every 1 = crotchet note

# notes = e f g a b c d and pause
d = {"d": 2,
     "c": 3,
     "b": 4,
     "a": 5,
     "g": 6,
     "f": 7,
     "e": 8,
     " ": 4}

from re import S
import sys

# function to record music from sequence
# also checks if input was appropriate
def record(line):
    i = 0
    notes = []
    count = 0
    while i < len(line):
        if line[i] in d.keys() and i % 2 == 0 or line[i].isnumeric():
            if not line[i].isnumeric():
                num = ""
                note = line[i]
            elif i != len(line) - 1:
                try:
                    if line[i + 1].isnumeric():
                        num = num + line[i]
                    else:
                        if num != "":
                            notes.append([note, int(num + line[i])])
                            count = count + int(num + line[i])
                        else:
                            notes.append([note, int(line[i])])
                            count = count + int(line[i])
                except UnboundLocalError:
                        print("The note '" + line[i] + "' is not valid. Valid notes are:",end="")
                        print(list(d.keys()))
                        i = i + 1
            else:
                if num != "":
                    notes.append([note, int(num + line[i])])
                    count = count + int(num + line[i])
                else:
                    notes.append([note, int(line[i])])
                    count = count + int(line[i])
        else:
            if i % 2 == 0:
                print("The note '" + line[i] + "' is not valid. Valid notes are:",end="")
                print(list(d.keys()))
                i = i + 1
            else:
                print("'" + line[i] + "' is not an integer.")
        i = i + 1

    # creating the staff (list of strings)
    block = ["-" * count, " " * count, "-" * count, " " * count, "-" * count, " " * count, "-" * count, " " * count, "-" * count]

    j = 0
    l = 0
    while j < len(notes):
        if notes[j][0] == " ":
            # pause
            sign = "#"
        else:
            #note
            sign = "@"

        # add to existing music
        block[d[notes[j][0]]] = block[d[notes[j][0]]][0:l] + sign * notes[j][1] + block[d[notes[j][0]]][notes[j][1] + l:]
        l = l + notes[j][1]
        j = j + 1

    return block

# bar class (list of strings)
class bar(object):
    # initialize 
    def __init__(self, notes = []):
        if notes != []:
            self.notes = record(notes)
        else:
            self.notes = notes

    def addBar(self, line):
        block = record(line)
        i = 0
        while i < len(self.notes):
            # append to existing strings
            self.notes[i] = self.notes[i] + block[i]
            i = i + 1
        if self.notes == []:
            # initialize if not already
            self.notes = block

    # formatted print
    def show(self):
        for line in self.notes:
            print(line, end="\n")

    # add spacing between each character of string
    def space(self, tab = 0):
        widen = []
        for k in self.notes:
            widen2 = []
            q = 0
            while q < len(k):
                widen2.append(k[q])
                q = q + 1
            indent = " " * tab
            widen.append(indent.join(widen2))

        self.notes = widen

    # set particular spacing between notes
    def unspace(self, tab = 0):
        notes = []
        for k in self.notes:
            line = "".join(k[::tab + 1])
            notes.append(line)
        self.notes = notes

    # remove a particular number of notes
    def delete(self, number):
        if number.isnumeric() and int(number) <= len(self.notes[0]):
            notes = []
            for k in self.notes:
                notes.append(k[:-int(number)])
            self.notes = notes

# execute the code
def main():
    # introduction message
    print("Welcome to Music Recorder, created by Krzysztof Baran." + "\n" + 'If you want a tutorial, please press "h".')
    print("You can record the notes: ",end='')
    print(list(d.keys()))
    s = False
    tab = 0

    # prints current bar
    def Show(Bar):
        if tab != 0:
            Bar.space(tab)
            Bar.show()
            Bar.unspace(tab)
        else:
            Bar.unspace(tab)
            Bar.show()

    line = 0
    while True:
        # end programme
        if line == "q":
            break

        # show help text
        elif line == "h":
            print("In this programme, every @ represents a quarter note while the # represents a break.")
            print('Press "c" to create new music.')
            print('Press "a" to add notes.')
            print('Press "d" to delete notes.')
            print('Press "q" to end the program.')
            print('Press "v" to view notes.')
            print('Press "s" to configure spacing.')
            print('Press "<" to view current music.')

        # reset bar
        elif line == "c":
            Bar = bar()
            print("Your music record is empty.")

        # add notes to current bar
        elif line == "a":
            print("Please press a note-digit sequence to add to current sheet music.")
            line = sys.stdin.readline().rstrip()
            try:
                Bar.addBar(line)
            except UnboundLocalError:
                Bar = bar(line)
            Show(Bar)

        # modify spacing
        elif line == "s":
            print("If you want spacing, press the desired number of spaces.")
            print("Pressing any other key will reset spacing to 0.")
            line = sys.stdin.readline().strip()
            if line.isnumeric():
                tab = int(line)
                s = True
            else:
                tab = 0
                s = False
            print("Your selected spacing is: " + str(tab) + " and your time signature is: " + "4" + "/4.")

        # model possible bar of interest
        elif line == "v":
            print("Please press a note-digit sequence to view. It won't be added to your current music record.")
            line = sys.stdin.readline().strip()
            Bar2 = bar(line)
            Show(Bar2)
            print('Please press "<" to view current music record.')

        # show recorded bar
        elif line == "<":
            try:
                Show(Bar)
            except UnboundLocalError:
                print("You have no music created to look back at.")

        # delete a number of notes
        elif line == "d":
            print("How many beats would you like to delete?")
            line = sys.stdin.readline().strip()
            Bar.delete(line)
            if Bar.notes == ['', '', '', '', '', '', '', '', '']:
                print("The recording has been fully deleted.")
            else:
                Show(Bar)

        # ask for new command
        print("\nPlease press a command. (h for help)\n")
        line = sys.stdin.readline().strip()
        print()
main()
