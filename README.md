# Music Record

This [Music Record programme](notes_record.py) allows you to record notes from a string format (eg. "a3c4b5") to a graphical format on the screen.

It is based on a series of possible commands in modifying the interface and record of notes:

* "c" to create new music.
* "a" to add notes.
* "d" to delete notes.
* "q" to end the program.
* "v" to view notes without recording them.
* "s" to configure spacing.
* "<" to view current recorded music.


Pauses may also be used.

Drawbacks: Only one octave is operational and bars aren't implemented.
